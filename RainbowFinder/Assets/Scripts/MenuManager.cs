﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public Button playButton;
    public Button exitButton;
    public Camera cam;

    public Color bgColor1 = Color.black;
    public Color bgColor2 = Color.gray;
    public float fadeDuration = 3.0F;

    // Start is called before the first frame update
    void Start()
    {
        playButton.onClick.AddListener(PlayGame);
        exitButton.onClick.AddListener(ExitGame);

        cam.clearFlags = CameraClearFlags.SolidColor;
    }

    // Update is called once per frame
    void Update()
    {
        float t = Mathf.PingPong(Time.time, fadeDuration) / fadeDuration;
        cam.backgroundColor = Color.Lerp(bgColor1, bgColor2, t);
    }

    void PlayGame()
    {
        GameManager.Instance.Clean();
        SceneManager.LoadScene("LevelOneScene");
    }

    void ExitGame()
    {
        Application.Quit();
    }

}
