﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public Button menuButton;
    public Text livesText;

    public int respawnDelay = 2;
    public Vector3 respawnPoint;

    public int lives = 5;

    private PlayerController player;

    // Start is called before the first frame update
    void Start()
    {
        menuButton.onClick.AddListener(BackToMenu);
        livesText.text = "Lives: " + lives;
        player = FindObjectOfType<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void BackToMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void Respawn(bool enemyHit = false){
      StartCoroutine("RespawnCoroutine", enemyHit);
    }

    public IEnumerator RespawnCoroutine(bool enemyHit = false){
        lives--;
        livesText.text = "Lives: " + lives;
        yield return new WaitForSeconds(respawnDelay);
        if(!enemyHit) player.transform.position = respawnPoint; 
        if(lives < 0){
            SceneManager.LoadScene("MainMenu");
        }
    }
}
