﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager instance = null;
    public static GameManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<GameManager>();
                if (instance == null)
                {
                    GameObject go = new GameObject();
                    go.name = "GameManager";
                    instance = go.AddComponent<GameManager>();
                    DontDestroyOnLoad(go);
                }
            }
            return instance;
        }
    }
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public bool green = false;
    public bool purpule = false;
    public bool orange = false;
    public bool red = false;
    public bool yellow = false;
    public bool blue = false;

    public void Clean()
    {
        green = false;
        purpule = false;
        orange = false;
        red = false;
        yellow = false;
        blue = false;
    }

    public void SetColor(string color)
    {
        if(color == "Green") green = true;
        if(color == "Purple") purpule = true;
        if(color == "Orange") orange = true;
        if(color == "Red") red = true;
        if(color == "Yellow") yellow = true;
        if(color == "Blue") blue = true;
    }

}
