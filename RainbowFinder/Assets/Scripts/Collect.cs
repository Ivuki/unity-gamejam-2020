﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Collect : MonoBehaviour
{
    void Start() { }

    void Update() { }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag != "Player") return;
        Image img = GameObject.Find(gameObject.tag + "Image").GetComponent("Image") as Image;
        img.enabled = true;
        GameManager.Instance.SetColor(gameObject.tag);
        Destroy(gameObject);
    }
}
