﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public int health = 2;

    private PlayerController playerController;

    private Animator enemyAnimator;

    // Start is called before the first frame update
    void Start()
    {
        playerController = FindObjectOfType<PlayerController>();
        enemyAnimator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        // Debug.Log(health);
        if(health < 0){
            gameObject.SetActive(false);
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player" && playerController.isAttacking){
          health--;
          enemyAnimator.SetBool("IsHit", true);
          StartCoroutine("HitDelay");
        }
    }

    void OnTriggerEnter2D(Collider2D collider){
          if(collider.gameObject.tag == "Player" && GameManager.Instance.orange){
              enemyAnimator.SetBool("Attack", true);
          }
    }
    void OnTriggerExit2D(Collider2D collider){
          if(collider.gameObject.tag == "Player"){
              enemyAnimator.SetBool("Attack", false);
          }
    }

    private IEnumerator HitDelay(){
        yield return new WaitForSecondsRealtime(1f);
        enemyAnimator.SetBool("IsHit", false);
    }
}
