﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private float ducking = 0f;

    public const int BlueLayer = 10;
    public const int GreenLayer = 11;

    private float movement = 0f;
    public float movementSpeed = 10f;

    private bool isJumping = false;
    private bool hasNotAirJumped = true;
    public float jumpSpeed = 40f;

    public bool isAttacking = false;

    private Rigidbody2D player;
    private Collider2D playerCollider;

    public Transform groundCheckPoint;
    public float groundCheckRadius = .5f;
    private bool isTouchingGround = false;

    public Transform ceilingCheckPoint;
    public float ceilingCheckRadius = .5f;
    private bool isTouchingCieling = false;

    public LayerMask groundLayer, blueLayer, greenLayer;
    private Collider2D groundLayerCollider, blueLayerCollider, greenLayerCollider;

    private Animator playerAnimator;

    private LevelManager levelManager;

    void Start()
    {
        player = GetComponent<Rigidbody2D>();
        playerCollider = GetComponent<Collider2D>();
        playerAnimator = GetComponent<Animator>();
        levelManager = FindObjectOfType<LevelManager>();
    }

    private void Update()
    {
        movement = Input.GetAxis("Horizontal");
        // Debug.Log(player.velocity);
        if (Input.GetButtonDown("Jump"))
        {
            if (!isTouchingGround)
            { //not Touching Ground
                if (GameManager.Instance.yellow && hasNotAirJumped)
                {
                    isJumping = true;
                    hasNotAirJumped = false;
                }
            }
            else
            {
                isJumping = true;
                hasNotAirJumped = true;
            }
        }

        if(Input.GetButtonDown("Fire3") && GameManager.Instance.red){
            isAttacking = true;
            StartCoroutine("DelayAttack");
        }


        //controlling animations:
        playerAnimator.SetBool("IsMoving", (player.velocity.x == 0) ? false : true);
        playerAnimator.SetBool("IsTouchingGround", isTouchingGround);
        playerAnimator.SetFloat("JumpSpeed", player.velocity.y);
        playerAnimator.SetBool("IsJumping", isJumping);
        // playerAnimator.SetBool("isTouchingCieling", isTouchingCieling);
        playerAnimator.SetBool("IsAttacking", isAttacking);

        if(GameManager.Instance.purpule){
            bool wasDucking = ducking == 0f ? false : true;

            ducking = Input.GetAxis("Vertical");
            playerAnimator.SetBool("IsDucking", ((ducking < 0) || isTouchingCieling) ? true : false);
            playerAnimator.SetBool("WasDucking", wasDucking || isTouchingCieling);
        }
    }

    void FixedUpdate()
    {
        isTouchingGround = checkLayerCollisions(groundCheckPoint, groundCheckRadius);
        isTouchingCieling = checkLayerCollisions(ceilingCheckPoint, ceilingCheckRadius);

        if (movement != 0f)
        {
            player.velocity = new Vector2(movement * movementSpeed, player.velocity.y);

            //scale of sprite
            float scale_x = Mathf.Abs(transform.localScale.x);
			      float scale_y = Mathf.Abs(transform.localScale.y);
            transform.localScale = new Vector2((movement > 0 ? 1 : -1) * scale_x, scale_y);
        }
        if (isJumping)
        {
            //delay first jump
            StartCoroutine(DelayJump());
            // player.velocity = new Vector2(player.velocity.x, jumpSpeed);
            isJumping = false; // Trows Race Condition if in Update
        }
    }
    //
    // Summary:
    //     Checks if we collided with platform layers.
    //
    // Returns:
    //   True if we collided with any platforms, false if we didn't
    private bool checkLayerCollisions(Transform checkPoint, float checkRadius)
    {
        groundLayerCollider = Physics2D.OverlapCircle(checkPoint.position, checkRadius, groundLayer);
        blueLayerCollider = Physics2D.OverlapCircle(checkPoint.position, checkRadius, blueLayer);
        greenLayerCollider = Physics2D.OverlapCircle(checkPoint.position, checkRadius, greenLayer);

        return _layerCollisionHelper(true, groundLayerCollider) ||
            _layerCollisionHelper(GameManager.Instance.blue, blueLayerCollider) ||
            _layerCollisionHelper(GameManager.Instance.green, greenLayerCollider);
    }
    //
    // Summary:
    //     Checks if the given collision should be ignored or not based on the color player has and the layer we are colliding with
    //
    // Parameters:
    //   hasColor:
    //     The color variable which player has;
    //
    //   layerCollider:
    //     TCollider of the layer items we want to compare with
    //
    // Returns:
    //   True if we collided with the specified layers platforms and it was not disabled
    private bool _layerCollisionHelper(bool hasColor, Collider2D layerCollider)
    {
        if (layerCollider)
        {
            bool isIgnored = Physics2D.GetIgnoreCollision(layerCollider, playerCollider);

            if (!hasColor && !isIgnored)
            {
                Physics2D.IgnoreCollision(layerCollider, playerCollider, true);
                return false;
            }
            else if (hasColor && isIgnored)
            {
                Physics2D.IgnoreCollision(layerCollider, playerCollider, false);
                return true;
            }

            return !isIgnored;
        }

        return false;
    }

    private IEnumerator DelayJump()
    {
        if(hasNotAirJumped){

            yield return new WaitForSecondsRealtime(0.4f);
        }
        player.velocity = new Vector2(player.velocity.x, jumpSpeed);
    }
    private IEnumerator DelayAttack(){
        yield return new WaitForSecondsRealtime(.4f);
        isAttacking = false;
    }

    void OnTriggerEnter2D(Collider2D collider){
        if(collider.gameObject.tag == "Rock"){
            levelManager.Respawn(true);
            Debug.Log("Dead");
        }

        if(collider.gameObject.tag == "FallDetector"){
            levelManager.Respawn();
            Debug.Log("Dead");
        }
    }
}
