﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneMenager : MonoBehaviour
{
    public Button menuButton;
    void Start()
    {
        menuButton.onClick.AddListener(BackToMenu);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
        void BackToMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
